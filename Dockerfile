FROM alpine:3.16

LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-prowlarr"

ARG BRANCH="develop"

WORKDIR /app/prowlarr/bin
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN \
  apk add --no-cache curl jq libintl sqlite-libs icu-libs xmlstarlet && \
  mkdir /config && \
    mkdir -p /app/prowlarr/bin && \
  PROWLARR_VERSION=$(curl -fsSL "https://prowlarr.servarr.com/v1/update/nightly/changes?os=linuxmusl&runtime=netcore&arch=x64" | jq -r .[0].version) && \
  export PROWLARR_VERSION && \
  curl -fsSL "https://prowlarr.servarr.com/v1/update/nightly/updatefile?version=${PROWLARR_VERSION}&os=linuxmusl&runtime=netcore&arch=x64" | tar xzf - -C "/app/prowlarr/bin" --strip-components=1 && \
  rm -rf "$HOME/.cache" "/var/cache/apk" "/app/prowlarr/bin/prowlarr.Update" "/tmp/*" "/var/tmp/*"

COPY /startup.sh /app/prowlarr/bin/startup.sh
   
# ports and volumes
EXPOSE 9696
VOLUME /config
ENTRYPOINT ["sh", "/app/prowlarr/bin/startup.sh"]

HEALTHCHECK --start-period=1m --timeout=5s --interval=1m \
    CMD /usr/bin/curl -fsSL "http://localhost:9696/system/about" && echo "OK" || exit 1

# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-prowlarr"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="prowlarr"
LABEL org.label-schema.schema-version="$PROWLARR_VERSION"
