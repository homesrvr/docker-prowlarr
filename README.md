# docker-prowlarr

[![pipeline status](https://gitlab.com/homesrvr/docker-prowlarr/badges/main/pipeline.svg)](https://gitlab.com/homesrvr/docker-prowlarr/commits/main) 
[![Prowlarr Release](https://gitlab.com/homesrvr/docker-prowlarr/-/jobs/artifacts/main/raw/release.svg?job=publish_badge)](https://gitlab.com/homesrvr/docker-prowlarr/-/jobs/artifacts/main/raw/release.txt?job=publish_badge)
[![Docker link](https://gitlab.com/homesrvr/docker-prowlarr/-/jobs/artifacts/main/raw/dockerimage.svg?job=publish_badge)](https://hub.docker.com/r/culater/prowlarr)

alpine-based dockerized build of prowlarr (development branch)
This is part of a collection of docker images, designed to run on my low-end x86 based QNAP NAS server. It is a lightweight image.

# Docker
The resulting docker image can be found here [https://hub.docker.com/r/culater/prowlarr](https://hub.docker.com/r/culater/prowlarr)

## Example usage

docker cli run example:
````
tbd
````

docker-compose example:
````
tbd
````
## Reporting problems
Please report any issues to the [Gitlab issue tracker](https://gitlab.com/homesrvr/docker-prowlarr/-/issues)

## Authors and acknowledgment
More information about radarr can be found here:
[radarr](https://prowlarr.com/ "Prowlarr Project Homepage") 


## Project status
The docker image auto-updates after a new (development) release of prowlarr within a few days. 