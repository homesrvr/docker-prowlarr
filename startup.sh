#!/usr/bin/with-contenv bash

cd /app/prowlarr/bin || exit

cat << EOF
'##::::'##::'#######::'##::::'##:'########:                    
 ##:::: ##:'##.... ##: ###::'###: ##.....::                    
 ##:::: ##: ##:::: ##: ####'####: ##:::::::                    
 #########: ##:::: ##: ## ### ##: ######:::                    
 ##.... ##: ##:::: ##: ##. #: ##: ##...::::                    
 ##:::: ##: ##:::: ##: ##:.:: ##: ##:::::::                    
 ##:::: ##:. #######:: ##:::: ##: ########:                    
..:::::..:::.......:::..:::::..::........::                    
:'######::'########:'########::'##::::'##:'########:'########::
'##... ##: ##.....:: ##.... ##: ##:::: ##: ##.....:: ##.... ##:
 ##:::..:: ##::::::: ##:::: ##: ##:::: ##: ##::::::: ##:::: ##:
. ######:: ######::: ########:: ##:::: ##: ######::: ########::
:..... ##: ##...:::: ##.. ##:::. ##:: ##:: ##...:::: ##.. ##:::
'##::: ##: ##::::::: ##::. ##:::. ## ##::: ##::::::: ##::. ##::
. ######:: ########: ##:::. ##:::. ###:::: ########: ##:::. ##:
:......:::........::..:::::..:::::...:::::........::..:::::..::                                    

EOF
                                                                         
exec /app/prowlarr/bin/Prowlarr -nobrowser -data=/config
